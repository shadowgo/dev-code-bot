import { User } from './user'
import { Project } from './project'
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm'

@Entity()
export class Subscription extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @ManyToOne(type => Project, project => project.subscribers)
    project: Project

    @ManyToOne(type => User, user => user.subscriptions)
    user: User

    @Column({ nullable: true })
    username: string
}
import { User } from '../models/user'
import { TelegrafContext } from 'telegraf/typings/context'

const greeting =
    'Welcome to Dev Code Bot 🤖️\n' +
    'Use /create_project to create a project\n' +
    'Use /subscribe to subscribe to a project\n' +
    'Use /unsubscribe to unsubscribe from a project\n' +
    'Use /get_projects to get list of your projects\n' +
    'Use /get_subscribtions to get list of your subscribtions'


export const start = async (ctx: TelegrafContext) => {
    let user = await User.findOne({ telegram_id: ctx.from?.id })
    if (!user) {
        user = new User()
        user.telegram_id = ctx.from?.id || -1
        user.chat_id = ctx.chat?.id || -1
        await user.save().then(() => {
            ctx.reply(greeting)
        }).catch((error) => {
            console.error(error)
        })
    } else {
        ctx.reply(greeting)
    }
}


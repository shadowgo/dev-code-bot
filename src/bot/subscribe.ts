import { Project } from '../models/project'
import { User, State } from '../models/user'
import { Subscription } from '../models/subscription'
import { TelegrafContext } from 'telegraf/typings/context'
import * as AsciiTable from 'ascii-table'

export const subscribe_start = async (ctx: TelegrafContext) => {
    const user = await User.findOne({ telegram_id: ctx.from?.id })
    if (!user) {
        ctx.reply('WTF? How the fuck are you not exist?')
    } else {
        user.state = State.SUBSCRIBE_START
        await user.save().then(() => {
            ctx.reply('Enter your project\'s token')
        }).catch((error) => {
            console.error(error)
        })
    }
}

export const subscribe_token = async (ctx: TelegrafContext, user: User) => {
    const project = await Project.findOne({ token: ctx.message?.text })
    if (!project) {
        ctx.reply('Project does not exists, are you sure the token is currect? Try entering it again!')
    } else {
        const subscription = new Subscription()
        subscription.project = project
        subscription.user = user
        await subscription.save().then(async (subscription) => {
            user.state = State.SUBSCRIBE_TOKEN
            user.state_helper_id = subscription.id
            await user.save().then(() => {
                ctx.reply('Enter the username/email/phone_number you use to login in your project')
            }).catch((error) => {
                console.error(error)
            })
        }).catch((error) => {
            console.error(error)
        })
    }
}

export const subscribe_username = async (ctx: TelegrafContext, user: User) => {
    const subscription = await Subscription.findOne({ id: user.state_helper_id })
    if (!subscription) {
        ctx.reply('Subscription does not exists, This should not happen!')
    } else {
        if (ctx.message && ctx.message.text && ctx.message.text.trim() !== '') {
            subscription.username = ctx.message.text
            await subscription.save().then(async () => {
                user.state = State.CLEAR
                user.state_helper_id = -1
                await user.save().then(() => {
                    ctx.reply('You are set and ready to go!')
                }).catch((error) => {
                    console.error(error)
                })
            }).catch((error) => {
                console.error(error)
            })
        } else {
            ctx.reply('Enter a valid username/email/phone_number')
        }
    }
}


export const unsubscribe_start = async (ctx: TelegrafContext) => {
    const user = await User.findOne({ telegram_id: ctx.from?.id }, { relations: ['subscriptions', 'subscriptions.project'] })
    if (!user) {
        ctx.reply('WTF? How the fuck are you not exist?')
    } else {
        user.state = State.UNSUBSCRIBE_START
        await user.save().then((user) => {
            if (user.subscriptions.length > 0) {
                const table = new AsciiTable()
                table.setHeading('id', 'project', 'username')
                user.subscriptions.forEach(item => {
                    table.addRow(item.id, item.project.name, item.username)
                })
                ctx.reply(
                    'These are your subscriptions, which one you want to unsubscribe?\n\n' +
                    '```' + table.toString() + '```',
                    { parse_mode: 'MarkdownV2' }
                )
            } else {
                ctx.reply('You have no subscriptions')
            }
        }).catch((error) => {
            console.error(error)
        })
    }
}

export const unsubscribe_id = async (ctx: TelegrafContext, user: User) => {
    if (ctx.message && ctx.message.text && !isNaN(Number(ctx.message.text))) {
        const subscription = await Subscription.findOne({ id: Number(ctx.message.text) }, { relations: ['user'] })
        if (subscription) {
            if (subscription.user.telegram_id == user.telegram_id) {
                const count = await Subscription.delete({ id: Number(ctx.message.text) })
                if (count.affected === 1) {
                    user.state = State.CLEAR
                    user.state_helper_id = -1
                    await user.save().then(() => {
                        ctx.reply('Subscription deleted')
                    }).catch((error) => {
                        console.error(error)
                    })
                }
            } else {
                // This use not owns this subscription but we don't tell
                // that it exists
                ctx.reply('Subscription not found')
            }
        } else {
            ctx.reply('Subscription not found')
        }
    } else {
        ctx.reply('Enter a valid id')
    }
}

export const get_subscribtions = async (ctx: TelegrafContext) => {
    const user = await User.findOne({ telegram_id: ctx.from?.id }, { relations: ['subscriptions', 'subscriptions.project'] })
    if (!user) {
        ctx.reply('WTF? How the fuck are you not exist?')
    } else {
        if (user.subscriptions.length > 0) {
            const table = new AsciiTable()
            table.setHeading('id', 'project', 'username')
            user.subscriptions.forEach((item) => {
                table.addRow(item.id, item.project.name, item.username)
            })
            ctx.reply('Your subscriptions are:\n\n' +
                '```' + table.toString() + '```',
                { parse_mode: 'MarkdownV2' }
            )
        } else {
            ctx.reply('You have no subscriptions, subscribe to a project using /subscribe')
        }
    }
}